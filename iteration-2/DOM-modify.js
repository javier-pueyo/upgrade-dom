/* 2.1 Inserta dinamicamente en un html un div vacio con javascript.

2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.

2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.

2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.

2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.

2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];

2.7 Elimina todos los nodos que tengan la clase .fn-remove-me

2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. 
	Recuerda que no solo puedes insertar elementos con .appendChild.

2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here */

function runJS() {
    console.log('run');
    const firstDiv = document.createElement("div");
    document.body.append(firstDiv);
    
    const fistP = document.createElement('p');    
    const secondDiv = document.createElement('div');    
    secondDiv.appendChild(fistP);
    document.body.append(secondDiv);

    const thirdDiv = document.createElement('div');
    for (let i = 0; i < 6; i++) {
        const newP = document.createElement('p');
        thirdDiv.appendChild(newP);
    }
    document.body.append(thirdDiv);

    const secondP = document.createElement('p');
    secondP.innerText = 'Soy dinámico!';
    document.body.append(secondP);

    document.querySelector('.fn-insert-here').innerText = 'Wubba Lubba dub dub';

    const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];
    const newUl = document.createElement('ul');
    apps.forEach(element => {
        const newLi = document.createElement('li');
        newLi.innerText = element;
        newUl.appendChild(newLi);
    });
    document.body.append(newUl);

    const removeNodes = document.querySelectorAll('.fn-remove-me');
    removeNodes.forEach(element => {
        element.remove();
    });

    const divInsert = document.querySelector('div');
    const pInMiddle = document.createElement('p');
    pInMiddle.innerText = 'Voy en medio!';
    divInsert.insertAdjacentElement('afterend', pInMiddle);

    const divwithP = document.querySelectorAll('div.fn-insert-here');
    divwithP.forEach(element => {
        const pInside = document.createElement('p');
        pInside.innerText = 'Voy dentro!';
        element.appendChild(pInside);
    });

}

window.onload = runJS;